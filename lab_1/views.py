from django.shortcuts import render
from datetime import datetime, date
import csv
# Enter your name here
mhs_name = ['Muhammad Rizki Darmawan', 'Anthony Dewa Priyasembada', 'Gusti Fahmi Fadhila'] # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = [date(1999, 9, 13), date(2001, 2, 22), date(1998, 7, 24)] #TODO Implement this, format (Year, Month, Date)
npm = [1706043765, 1706979171, 1706044036]  # TODO Implement this
study = "Universitas Indonesia, Depok"
hobby = ["Menonton film dan membaca novel atau komik.", "Main voli", "Main game"]
description = ["Salah satu dari 10 Pemuda Soekarno.", "Masih muda, pendiem, ngomong a cuman dikit", "Memiliki suatu kharisma dengan tersendirinya"]
# Create your views here.
def index(request):
    response = {'my_name': mhs_name[0], 'name1': mhs_name[1], 'name2': mhs_name[2],
	'my_age': calculate_age(birth_date[0].year), 'age1': calculate_age(birth_date[1].year), 'age2': calculate_age(birth_date[2].year),
	'my_npm': npm[0], 'npm1': npm[1], 'npm2': npm[2],
	'study': study, 
	'my_hobby': hobby[0], 'hobby1': hobby[1], 'hobby2': hobby[2], 
	'my_description': description[0], 'description1': description[1], 'description2': description[2]}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
